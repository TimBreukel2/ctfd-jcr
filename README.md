Dit moet de repo worden voor de publieke versie van de CTFd deployment waarin onze plugins zijn opgenomen. Resultaat is een container in een publieke registry. 
Er mogen dus geen secrets van betekenis instaat. Uitzondering wellicht voor de DB connection string, als dat niet intelligent kan (doe niet teveel moeite)

De readme van de oorspronkele ctfd van ons is te uitgebreid. 
Er zijn twee use cases: gebruik de container in getting-started respectievelijk cluster. (eenvoudig)
tweede case: hoe ontwikkel/test ik deze deployment als ik a: updates wil doen, b: feature wijzigingen wil doen. 

# done
- devcontainer werkend
- dockerfile
- requirements juiste versies (voor nu)
- 2 custom plugins werkend
- container_challenges 
- initial_setup 
- 2 aangepaste plugins bijwerken
- dynamic_challenges
- challenges
- documentatie bijwerken

# to do
- 1 custom plugins werkend krijgen
- custom_auth 

# Joint Cyber Range Development Guide

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/hu-hc/jcr/platform/ctf-platform)

[![pipeline status](https://gitlab.com/elcortegano/metapop2/badges/master/pipeline.svg)](https://gitlab.com/hu-hc/jcr/platform/ctf-platform/-/commits/master)

## 1. Important Sources

- [Main Website](https://jointcyberrange.nl/)
- [JCR documentation](https://docs.jointcyberrange.nl/)
- [Developer FAQ](readme/FAQ.md)
- [Repository overview](readme/OVERVIEW.md)

## 2. Live Preview

- [hu.jointcyberrange.nl](https://hu.jointcyberrange.nl)
- [demo.jointcyberrange.nl](https://demo.jointcyberrange.nl)
- [dev-jointcyberrange.nl](https://dev-jointcyberrange.nl)

## 3. Technology Stack

- HTML
- CSS
- JavaScript
- Python
- SQL

## 4. Development outside Kubernetes

This is a guide with all the steps to deploy CTFd locally. There are a few applications that needs to be installed, if you already have these installed, the steps can be skipped.

The git pipeline with this repo creates a docker image in the gitlab repo. To run that locally, consult [this repo](https://gitlab.com/pveijk/ctfd-localk8).

### 4.1 Docker Desktop

To use Docker Desktop you need to have WSL 2 or Hyper-V enabled on your PC, depending on your Windows version. You also need to have hardware virtualization enabled in your BIOS settings. Instructions can be found [here](https://docs.docker.com/docker-for-windows/install).
Equivalent instructions for Mac are [here](https://docs.docker.com/docker-for-mac/install/).

1. Download Docker Desktop [here](https://www.docker.com/products/docker-desktop).
2. Double-click **Docker Desktop Installer.exe** to run the installer.
3. When you are on the configuration page, make sure the **Enable Hyper-V Windows Features** or the **Install required Windows components for WSL 2** option is selected.
4. Follow the instructions and complete the installation process.
5. Start Docker Desktop.
6. Go to Settings > Kubernetes and make sure **Enable Kubernetes** is checked.

### 4.2 Visual Studio Code

1. Download Visual Studio Code [here](https://code.visualstudio.com/download).
2. Double-click **VSCodeUserSetup-{version}.exe** to run the installer.
3. Follow the instructions and complete the installation process.
4. Start Visual Studio Code.
5. Go to Extensions in the left sidebar and search for **Remote - Containers**. Install this extension and make sure it is enabled.

### 4.3 Preparing CTFd

1. Make sure Docker Desktop is running, including Kubernetes.
2. In Visual Studio Code go to Source Control in the left sidebar and click **Clone Repository**. Paste this repository link in the search bar and hit enter. Choose a file location where you want to save the repository files on your PC.
3. In Visual Studio Code, press F1 and type in the bar **Remote-Containers: Open Folder in Container** and select the repository folder. The container will be built according to the specs in ```.devcontainer```.
4. Specify the mandatory CTFd setup ENV variables they can be found [here](CTFd/config.ini). An in-depth explanation of every variable can be found [here](CTFd/plugins/initial_setup/readme/ENV_VARIABLES.md).

### 4.4 Running CTFd
1. Open the Terminal in Visual Code Studio to use the container.
2. Now type in ```flask run``` to run the CTFd. In the Terminal you can see the URL (<http://127.0.0.1:4000>). Open this link in your browser.

## 5. Development in Kubernetes with Tilt

Tilt creates an abstraction for developers on top off Kubernetes. This makes Kubernetes development less complicated. Features like the Web-UI and logging, make it less painful to locate issues. Tilt innovates on top off the idea of Skaffold, the original project enabling live code updates inside Kubernetes pods. Tilt is very flexible in its configuration, due to the use of the Starlark Python dialect. The `Tiltfile` is used for configuration.

### 5.1 Installation (moet worden nagekeken)

Install Tilt for your system with the following [link](https://docs.tilt.dev/install.html). 

**Linux**:

```Bash
curl -fsSL https://raw.githubusercontent.com/tilt-dev/tilt/master/scripts/install.sh | bash
```

Install the ingress NGINX controller:
```Bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.0/deploy/static/provider/cloud/deploy.yaml
```

We want to spin up a CTFd development environment inside Kubernetes. Use the following command from within the main repository's directory to start Tilt:

```Bash
tilt up
```

Press the `space bar` to open the Web-UI and `S` to show logs, however logs are also shown in the Web-UI.

When Tilt is done building and preparing the application for you, it will be available at [kubernetes.docker.internal](http://kubernetes.docker.internal). For using CTFd, register an user with the email `jointcyberrange@admin.nl` to become admin.

When done with Tilt, delete its resources as follows:

```Bash
tilt down
```

By default CTFd is deployed without persistent database. You could change this by uncommenting the following line in the `Tiltfile`.

```Bash
# k8s_yaml('k8s/tilt-data.yaml')
```

And comment the following.

```Bash
k8s_yaml('k8s/tilt-minimal.yaml')
```

### 5.2 Troubleshooting

When you can not run the Bash script, make sure it has the correct permissions to be executable.

```Bash
chmod +x ./k8s/dev/clean-tilt-initial-k8s-setup.sh
```

If Tilt doesn't show the UI when the `spacebar` is pressed after `tilt up`. Delete the binary and install it again.

```Bash
sudo rm $(which tilt)
curl -fsSL https://raw.githubusercontent.com/tilt-dev/tilt/master/scripts/install.sh | bash
```

## 6. Credits
- [CTFd](https://github.com/CTFd/CTFd) contributors
- Students of the Joint Cyber Range


## links
[getting started](https://gitlab.com/jointcyberrange.nl/getting-started/-/blob/main/README.md)