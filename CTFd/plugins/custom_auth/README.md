# Custom Auth Plugin

Welcome, this documentation file will explain the custom auth plugin both briefly and thoroughly. The custom auth plugin is a plugin that manages the authentication and authorization of the JCR (hence the name "auth"). It handles things like: register, login, password reset, etc. This plugin serves as both a service provider (SURFconext account) and identity provider (CTFd account). Multiple technologies are used to make this plugin work, as listed in the `Used Technologies/Concepts` section.

## 1. Features

- CTFd account register
- CTFd account verification
- CTFd account login
- CTFd account password reset
- SURFconext account login

## 2. Happy Flows

### 2.1 CTFd Register

> - Plugin initializes itself
> - User wants to register
> - User fills in credentials
> - Plugin validates credentials
> - Plugin creates account
> - Plugin sends account verification email
> - User clicks URL in email
> - Plugin verifies account

### 2.2 CTFd Login

> - Plugin initializes itself
> - User wants to login
> - User fills in credentials
> - Plugin validates credentials
> - Plugin logs in account

### 2.3 CTFd Password Reset

> - Plugin initializes itself
> - User wants to reset password
> - User fills in account email
> - Plugin sends password reset email
> - User clicks URL in email
> - User fills in new password
> - Plugin changes password

### 2.4 SURFconext Login

> - Plugin initializes itself
> - User wants to login
> - Plugin sends user to SP
> - SP sends user to IDP login page
> - User fills in credentials
> - IDP validates credentials
> - IDP redirects user back to SP
> - SP redirects user back to plugin
> - Plugin extracts SURFconext account details
> - Plugin creates or reuses CTFd account
> - Plugin logs in CTFd account

## 3. Technical Documentation

- [SURFconext](readme/SURFCONEXT.md)
- [ENV Variables](readme/ENV_VARIABLES.md)
- [Plugin Structure](readme/PLUGIN_STRUCTURE.md)

## 4. Abbreviations

- JCR = Joint Cyber Range
- CTF = Capture The Flag
- HU = Hogeschool Utrecht
- THUAS = The Hague University of Applied Sciences
- IDP = Identity Provider
- SP = Service Provider
- SSO = Single Sign On
- OIDC = OpenID Connect
- HTML = HyperText Markup Language
- CSS = Cascading Style Sheets
- OAuth = Open Authorization
- ENV = Environment

## 5. Used Technologies/Concepts

- [CTFd](https://ctfd.io/)
- [Python](https://en.wikipedia.org/wiki/Python_(programming_language))
- [Flask](https://en.wikipedia.org/wiki/Flask_(web_framework))
- [HTML](https://en.wikipedia.org/wiki/HTML)
- [CSS](https://en.wikipedia.org/wiki/CSS)
- [OAuth](https://en.wikipedia.org/wiki/OAuth)
- [SAML](https://en.wikipedia.org/wiki/Security_Assertion_Markup_Language)
- [OpenID](https://en.wikipedia.org/wiki/OpenID)
- [IDP](https://en.wikipedia.org/wiki/Identity_provider)
- [SP](https://en.wikipedia.org/wiki/Service_provider)
- [SSO](https://en.wikipedia.org/wiki/Single_sign-on)
- [Federated Identity](https://en.wikipedia.org/wiki/Federated_identity)
- [Session Fixation](https://en.wikipedia.org/wiki/Session_fixation)
- [Happy Flow](https://en.wikipedia.org/wiki/Happy_path)
- [Environment Variable](https://en.wikipedia.org/wiki/Environment_variable)
